//
//  WeatherManager.swift
//  Clima
//
//  Created by Oscar on 2022-11-01.
//  Copyright © 2022 App Brewery. All rights reserved.
//

import Foundation

protocol WeatherManagerDelegado {
    func mostrarClimaCiudad(lista: WeatherData)
    func obtenerClimaCiudad(climaDatos: WeatherData)
}

struct WeatherManager {
    var delegado: WeatherManagerDelegado?
    
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=f44c7dd937367fc5137a5f3aa6a12e7c"
    
    mutating func obtenerClima(nombreCiudad: String) {
        let urlString = "\(weatherURL)&q=\(nombreCiudad)"
        performRequest(urlString: urlString)
    }
    
    func performRequest(urlString: String) {
        // creamos url
        // hacemos un if let porque la url puede crearse mal por alguna razon
        // entonces para no tener un URL? usamos if let
        if let url = URL(string: urlString){
            // creamos una URLSession
            let session = URLSession(configuration: .default)
            // damos una task a la session
            let task = session.dataTask(with: url) { datos, respuesta, error in
                if error != nil {
                    print("Error al realizar la peticion: ", error?.localizedDescription)
                }
                
                if let parsedData = parseJSON(climaData: datos!) {
                    print(parsedData)
                    delegado?.obtenerClimaCiudad(climaDatos: parsedData)
                }
            }
            // ejecutamos la task
            task.resume()
            
        }
    }
    
    func parseJSON(climaData: Data) -> WeatherData? {
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(WeatherData.self, from: climaData)
            return decodedData
        } catch {
            print(error)
            return nil
        }
    }
}
