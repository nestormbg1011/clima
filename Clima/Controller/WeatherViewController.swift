//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherManager = WeatherManager()
    var climaCiudad: WeatherData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherManager.delegado = self
        searchTextField.delegate = self
        weatherManager.obtenerClima(nombreCiudad: "Asuncion")
    }

    /// el textField `textField` nos avisa que se apretó el botón enter.
    /// Y nos pregunta si tiene que retornar. Le damos la respueta retornando una expresión Booleana (true o false)
    @IBAction func search(_ sender: UIButton) {
        print("se apretó buscar!")
        self.searchTextField.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("se apretó enter!")
        self.searchTextField.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let ciudad = textField.text {
            weatherManager.obtenerClima(nombreCiudad: ciudad)
        }
        
        searchTextField.text = ""
    }
}

extension WeatherViewController:  WeatherManagerDelegado {
    func mostrarClimaCiudad(lista: WeatherData) {
    }
    
    func obtenerClimaCiudad(climaDatos: WeatherData) {
        climaCiudad = climaDatos
        let temperatura = climaCiudad!.main.temp
        let ciudad = climaCiudad!.name
        DispatchQueue.main.async {
            self.temperatureLabel.text = String(Int(temperatura - 273.15))
            self.cityLabel.text = ciudad
        }
    }
}


